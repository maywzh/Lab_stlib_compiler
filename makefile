.PHONY: build test clean

build: libmylib.a

libmylib.a: math.o print.o
	ar crv $@ math.o print.o

math.o: math.c
	gcc -c math.c

print.o: print.c
	gcc -c print.c

test: test.c
	gcc test.c -L. -lmylib -o test
clean: 
	rm -f *.o *.so *.a test